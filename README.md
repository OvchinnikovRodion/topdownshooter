# TopDownShooter

## Используемые технологии
- Niagara Visual Effects
- Animation Blueprints
- Animation Montage
- Blend Spaces and State Machines
- Character Controller
- UMG UI

## Функционал
- Уровень с меню и два игровых уровня
- Подбираемое оружие и патроны
- Эффекты состояний персонажа

