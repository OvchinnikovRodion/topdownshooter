#include "InventoryComponent.h"
#include "../Game/TopDownShooterGameInstance.h"

UInventoryComponent::UInventoryComponent()
{
	PrimaryComponentTick.bCanEverTick = true;
}

void UInventoryComponent::BeginPlay()
{
	Super::BeginPlay();
}

void UInventoryComponent::TickComponent(float DeltaTime, ELevelTick TickType, FActorComponentTickFunction* ThisTickFunction)
{
	Super::TickComponent(DeltaTime, TickType, ThisTickFunction);
}

void UInventoryComponent::SwitchToWeapon_OnServer_Implementation(FName weaponName)
{
	OnSwitchWeapon.Broadcast(weaponName);
}

void UInventoryComponent::AddWeapon(FName weaponName)
{
	UTopDownShooterGameInstance* gameInsance = Cast<UTopDownShooterGameInstance>(GetWorld()->GetGameInstance());
	if (gameInsance)
	{
		FWeaponInfo weaponInfo;
		if (gameInsance->GetWeaponInfoByName(weaponName, weaponInfo))
		{
			if (weaponInfo.WeaponClass)
			{
				Weapons.Add(weaponName, weaponInfo);
				FAmmoInfo ammoInfo;
				ammoInfo.MaxCartridgesInInventory = weaponInfo.MaxAmmoInInventory;
				Ammo.Add(weaponName, ammoInfo);
			}
		}
	}
}

void UInventoryComponent::AddCartridges(FName weaponName, int quantity)
{
	UE_LOG(LogTemp, Warning, TEXT("Add ammo to weapon %s"), *weaponName.ToString());
	FAmmoInfo* ammoInfo = Ammo.Find(weaponName);

	if (ammoInfo)
	{
		ammoInfo->CartridgesAmount += quantity;

		if (ammoInfo->CartridgesAmount > ammoInfo->MaxCartridgesInInventory)
		{
			ammoInfo->CartridgesAmount = ammoInfo->MaxCartridgesInInventory;
		}

		OnCartridgesAdded.Broadcast(weaponName);
	}
}