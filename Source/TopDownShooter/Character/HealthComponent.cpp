#include "../Character/HealthComponent.h"
#include "Net/UnrealNetwork.h"

UHealthComponent::UHealthComponent()
{
	PrimaryComponentTick.bCanEverTick = true;
}

void UHealthComponent::BeginPlay()
{
	Super::BeginPlay();
}

void UHealthComponent::TickComponent(float DeltaTime, ELevelTick TickType, FActorComponentTickFunction* ThisTickFunction)
{
	Super::TickComponent(DeltaTime, TickType, ThisTickFunction);
}

float UHealthComponent::GetCurrentHealth()
{
	return Health;
}

void UHealthComponent::SetCurrentHealth(float NewHealth)
{
	Health = NewHealth;
}

void UHealthComponent::ChangeHealth(float ChangeValue)
{
	UE_LOG(LogTemp, Warning, TEXT("UHealthComponent::ChangeHealth - ChangeHealth"));
	if (IsAlive)
	{
		ChangeValue = ChangeValue * CoefDamage;
		Health += ChangeValue;

		if (Health > 100.0f)
		{
			ChangeValue -= Health - 100.0f;
			Health = 100.0f;
		}

		OnHealthChange.Broadcast(Health, ChangeValue);

		if (Health < 0.0f)
		{
			IsAlive = false;
			UE_LOG(LogTemp, Warning, TEXT("UHealthComponent::ChangeHealth - OnDead.Broadcast()"));
			OnDead.Broadcast();
		}
	}
}

void UHealthComponent::OnRep_Health()
{
	OnHealthChange.Broadcast(Health, 0.0f);
}

void UHealthComponent::GetLifetimeReplicatedProps(TArray<FLifetimeProperty>& OutLifetimeProps) const
{
	Super::GetLifetimeReplicatedProps(OutLifetimeProps);

	DOREPLIFETIME(UHealthComponent, Health);
}
