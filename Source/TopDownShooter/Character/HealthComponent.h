#pragma once

#include "CoreMinimal.h"
#include "Components/ActorComponent.h"
#include "HealthComponent.generated.h"

DECLARE_DYNAMIC_MULTICAST_DELEGATE_TwoParams(FOnHealthChange, float, Health, float, Damage);
DECLARE_DYNAMIC_MULTICAST_DELEGATE(FOnDead);

UCLASS( ClassGroup=(Custom), meta=(BlueprintSpawnableComponent) )
class TOPDOWNSHOOTER_API UHealthComponent : public UActorComponent
{
	GENERATED_BODY()

public:
UPROPERTY(BlueprintAssignable, Category = "Health")
	FOnHealthChange OnHealthChange;
UPROPERTY(BlueprintAssignable, Category = "Health")
	FOnDead OnDead;

	UHealthComponent();
	virtual void TickComponent(float DeltaTime, ELevelTick TickType, FActorComponentTickFunction* ThisTickFunction) override;
UFUNCTION(BlueprintCallable, Category = "Health")
	float GetCurrentHealth();
UFUNCTION(BlueprintCallable, Category = "Health")
	void SetCurrentHealth(float NewHealth);
UFUNCTION(BlueprintCallable, Category = "Health")
	virtual void ChangeHealth(float ChangeValue);

UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Health")
	float CoefDamage = 1.0f;
UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = "Health")
	bool IsAlive = true;
UFUNCTION()
	void OnRep_Health();

protected:
	virtual void BeginPlay() override;
UPROPERTY(ReplicatedUsing = OnRep_Health)
	float Health = 100.0f;
};
