#include "../Character/CharacterHealthComponent.h"

void UCharacterHealthComponent::ChangeHealth(float ChangeValue, bool IgnoreShield)
{
	if (ChangeValue < 0.0f && IgnoreShield == false)
	{
		float appliedDamage = ChangeValue * CoefDamage;

		if (Shield > -appliedDamage)
		{
			ChangeShieldValue(appliedDamage);
			return;
		}
		else
		{
			float difference = Shield + appliedDamage;
			ChangeShieldValue(-Shield);
			appliedDamage = difference;
			Super::ChangeHealth(difference);
			return;
		}
	}

	Super::ChangeHealth(ChangeValue);
}

void UCharacterHealthComponent::ChangeHealth(float ChangeValue)
{
	ChangeHealth(ChangeValue, false);
}

void UCharacterHealthComponent::ChangeShieldValue(float ChangeValue)
{
	Shield += ChangeValue;
	OnShieldChange.Broadcast(Shield, ChangeValue);

	if (Shield > 100.0f)
	{
		Shield = 100.0f;
	}
	else
	{
		if (Shield < 0.0f)
			Shield = 0.0f;
	}

	if (GetWorld())
	{
		GetWorld()->GetTimerManager().SetTimer(TimerHandle_CollDownShieldTimer, this, &UCharacterHealthComponent::CoolDownShieldEnd, CoolDownShieldRecoverTime, false);
		GetWorld()->GetTimerManager().ClearTimer(TimerHandle_ShieldRecoveryRateTimer);
	}
}

void UCharacterHealthComponent::CoolDownShieldEnd()
{
	if (GetWorld())
	{
		if (IsAlive)
		{
			GetWorld()->GetTimerManager().SetTimer(TimerHandle_ShieldRecoveryRateTimer, this, &UCharacterHealthComponent::RecoveryShield, ShieldRecoverRate, true);
		}
		else
		{
			GetWorld()->GetTimerManager().PauseTimer(TimerHandle_ShieldRecoveryRateTimer);
		}
	}
}

void UCharacterHealthComponent::RecoveryShield()
{
	float tmp = Shield;
	tmp = tmp + ShieldRecoverValue;
	if (tmp > 100.0f)
	{
		Shield = 100.0f;
		if (GetWorld())
		{
			GetWorld()->GetTimerManager().ClearTimer(TimerHandle_ShieldRecoveryRateTimer);
		}
	}
	else
	{
		Shield = tmp;
	}

	OnShieldChange.Broadcast(Shield, ShieldRecoverValue);
}

float UCharacterHealthComponent::GetShieldValue()
{
	return Shield;
}
