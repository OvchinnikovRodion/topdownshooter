#include "EnemyCharacter.h"
#include "GameFramework/CharacterMovementComponent.h"

AEnemyCharacter::AEnemyCharacter()
{
	PrimaryActorTick.bCanEverTick = true;

	HealthComponent = CreateDefaultSubobject<UHealthComponent>(TEXT("CharacterHealthComponent"));
	if (HealthComponent)
	{
		HealthComponent->OnDead.AddDynamic(this, &AEnemyCharacter::CharacterDead);
	}

	GetCharacterMovement()->bConstrainToPlane = true;
	GetCharacterMovement()->bSnapToPlaneAtStart = true;
}

void AEnemyCharacter::BeginPlay()
{
	Super::BeginPlay();
}

void AEnemyCharacter::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);
}

void AEnemyCharacter::CharacterDead()
{
	CanMove = false;
	CanAttack = false;
	GetCharacterMovement()->StopMovementImmediately();

	AController* AIController = GetController();
	if (AIController)
	{
		AIController->UnPossess();
		AIController->Destroy();
	}
	
	SetLifeSpan(20.0f);
	float timeOffset = 0.5f;
	float deadAnimationTime = 0.0f;
	int32 rnd = FMath::RandHelper(DeathAnimations.Num());
	UAnimInstance* animInstance = GetMesh()->GetAnimInstance();
	if (DeathAnimations[rnd] && animInstance)
	{
		deadAnimationTime = DeathAnimations[rnd]->GetPlayLength();
		animInstance->Montage_Play(DeathAnimations[rnd]);
	}

	GetWorldTimerManager().SetTimer(TimerHandle_RagDollTimer, this, &AEnemyCharacter::EnableRagdoll, deadAnimationTime-timeOffset, false);

	CharacterDead_BP();
}

void AEnemyCharacter::CharacterDead_BP_Implementation()
{
	//BP
}

void AEnemyCharacter::EnableRagdoll()
{
	if (GetMesh())
	{
		GetMesh()->SetCollisionEnabled(ECollisionEnabled::PhysicsOnly);
		GetMesh()->SetSimulatePhysics(true);
	}
}

float AEnemyCharacter::TakeDamage(float DamageAmount, struct FDamageEvent const& DamageEvent, class AController* EventInstigator, AActor* DamageCauser)
{
	float ActualDamage = Super::TakeDamage(DamageAmount, DamageEvent, EventInstigator, DamageCauser);
	if (HealthComponent->IsAlive)
	{
		HealthComponent->ChangeHealth(-DamageAmount);
	}

	return ActualDamage;
}