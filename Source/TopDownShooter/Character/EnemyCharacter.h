#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Character.h"
#include "../Interfaces/IEffectsSubject.h"
#include "HealthComponent.h"
#include "EnemyCharacter.generated.h"

UCLASS(Blueprintable)
class TOPDOWNSHOOTER_API AEnemyCharacter : public ACharacter, public IEffectsSubject
{
	GENERATED_BODY()

public:
	AEnemyCharacter();
	virtual void Tick(float DeltaSeconds) override;

UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = "Health")
	class UHealthComponent* HealthComponent;
UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Animations")
	TArray<UAnimMontage*> DeathAnimations;
UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Animations")
	TArray<UAnimMontage*> AttackAnimations;

UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Movement")
	bool CanMove = true;
UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Combat")
	bool CanAttack = true;

UFUNCTION()
	void CharacterDead();
UFUNCTION(BlueprintNativeEvent)
	void CharacterDead_BP();
	virtual float TakeDamage(float DamageAmount, struct FDamageEvent const& DamageEvent, class AController* EventInstigator, AActor* DamageCauser) override;

protected:
	virtual void BeginPlay() override;
	void EnableRagdoll();
	FTimerHandle TimerHandle_RagDollTimer;
};