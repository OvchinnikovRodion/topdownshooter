// Copyright Epic Games, Inc. All Rights Reserved.

#include "TopDownShooterCharacter.h"
#include "UObject/ConstructorHelpers.h"
#include "AnimGraphRuntime/Public/KismetAnimationLibrary.h"
#include "Camera/CameraComponent.h"
#include "Components/DecalComponent.h"
#include "Components/CapsuleComponent.h"
#include "Engine/World.h"
#include "GameFramework/CharacterMovementComponent.h"
#include "GameFramework/PlayerController.h"
#include "GameFramework/SpringArmComponent.h"
#include "Kismet/GameplayStatics.h"
#include "Kismet/KismetMathLibrary.h"
#include "Materials/Material.h"
#include "Net/UnrealNetwork.h"
#include "NiagaraFunctionLibrary.h"
#include "NiagaraComponent.h"
#include "../Game/TopDownShooterGameInstance.h"


ATopDownShooterCharacter::ATopDownShooterCharacter()
{
	// Set size for player capsule
	GetCapsuleComponent()->InitCapsuleSize(42.f, 96.0f);

	// Don't rotate character to camera direction
	bUseControllerRotationPitch = false;
	bUseControllerRotationYaw = false;
	bUseControllerRotationRoll = false;

	// Configure character movement
	GetCharacterMovement()->bOrientRotationToMovement = true; // Rotate character to moving direction
	GetCharacterMovement()->RotationRate = FRotator(0.f, 640.f, 0.f);
	GetCharacterMovement()->bConstrainToPlane = true;
	GetCharacterMovement()->bSnapToPlaneAtStart = true;

	// Create a camera boom...
	CameraBoom = CreateDefaultSubobject<USpringArmComponent>(TEXT("CameraBoom"));
	CameraBoom->SetupAttachment(RootComponent);
	CameraBoom->SetUsingAbsoluteRotation(true); // Don't want arm to rotate when character does
	CameraBoom->TargetArmLength = 800.f;
	CameraBoom->SetRelativeRotation(FRotator(-60.f, 0.f, 0.f));
	CameraBoom->bDoCollisionTest = false; // Don't want to pull camera in when it collides with level

	// Create a camera...
	TopDownCameraComponent = CreateDefaultSubobject<UCameraComponent>(TEXT("TopDownCamera"));
	TopDownCameraComponent->SetupAttachment(CameraBoom, USpringArmComponent::SocketName);
	TopDownCameraComponent->bUsePawnControlRotation = false; // Camera does not rotate relative to arm

	InventoryComponent = CreateDefaultSubobject<UInventoryComponent>(TEXT("InventoryComponent"));
	if (InventoryComponent)
	{
		InventoryComponent->OnSwitchWeapon.AddDynamic(this, &ATopDownShooterCharacter::SwitchToWeapon);
		InventoryComponent->OnCartridgesAdded.AddDynamic(this, &ATopDownShooterCharacter::HandleCartridgesAddition);
	}
	CharacterHealthComponent = CreateDefaultSubobject<UCharacterHealthComponent>(TEXT("CharacterHealthComponent"));
	if (CharacterHealthComponent)
	{
		CharacterHealthComponent->OnDead.AddDynamic(this, &ATopDownShooterCharacter::CharacterDead);
	}
	PawnNoiseEmitterComponent = CreateDefaultSubobject<UPawnNoiseEmitterComponent>(TEXT("PawnNoiseEmitterComponent"));
	// Activate ticking in order to update the cursor every frame.
	PrimaryActorTick.bCanEverTick = true;
	PrimaryActorTick.bStartWithTickEnabled = true;

	//NetWork 
	bReplicates = true;
}

void ATopDownShooterCharacter::Tick(float DeltaSeconds)
{
    Super::Tick(DeltaSeconds);

	MovementTick(DeltaSeconds);
}

void ATopDownShooterCharacter::BeginPlay()
{
	Super::BeginPlay();
}

void ATopDownShooterCharacter::SetupPlayerInputComponent(UInputComponent* inputComponent)
{
	Super::SetupPlayerInputComponent(inputComponent);

	inputComponent->BindAxis(TEXT("MoveForward"), this, &ATopDownShooterCharacter::InputAxisX);
	inputComponent->BindAxis(TEXT("MoveRight"), this, &ATopDownShooterCharacter::InputAxisY);

	inputComponent->BindAction(TEXT("LeftShift"), IE_Pressed, this, &ATopDownShooterCharacter::LeftShiftPressed);
	inputComponent->BindAction(TEXT("LeftShift"), IE_Released, this, &ATopDownShooterCharacter::LeftShiftReleased);

	inputComponent->BindAction(TEXT("FireEvent"), EInputEvent::IE_Pressed, this, &ATopDownShooterCharacter::InputAttackPressed);
	inputComponent->BindAction(TEXT("FireEvent"), EInputEvent::IE_Released, this, &ATopDownShooterCharacter::InputAttackReleased);
	inputComponent->BindAction(TEXT("AimEvent"), EInputEvent::IE_Pressed, this, &ATopDownShooterCharacter::StartAiming);
	inputComponent->BindAction(TEXT("AimEvent"), EInputEvent::IE_Released, this, &ATopDownShooterCharacter::StopAiming);
	inputComponent->BindAction(TEXT("ReloadEvent"), EInputEvent::IE_Released, this, &ATopDownShooterCharacter::TryReloadWeapon);
	
	TArray<FKey> HotKeys;
	HotKeys.Add(EKeys::Zero);
	HotKeys.Add(EKeys::One);
	HotKeys.Add(EKeys::Two);
	HotKeys.Add(EKeys::Three);
	HotKeys.Add(EKeys::Four);
	HotKeys.Add(EKeys::Five);
	HotKeys.Add(EKeys::Six);
	HotKeys.Add(EKeys::Seven);
	HotKeys.Add(EKeys::Eight);
	HotKeys.Add(EKeys::Nine);
	
	inputComponent->BindKey(HotKeys[0], IE_Pressed, this, &ATopDownShooterCharacter::NumberKeyPressed<0>);
	inputComponent->BindKey(HotKeys[1], IE_Pressed, this, &ATopDownShooterCharacter::NumberKeyPressed<1>);
	inputComponent->BindKey(HotKeys[2], IE_Pressed, this, &ATopDownShooterCharacter::NumberKeyPressed<2>);
	inputComponent->BindKey(HotKeys[3], IE_Pressed, this, &ATopDownShooterCharacter::NumberKeyPressed<3>);
	inputComponent->BindKey(HotKeys[4], IE_Pressed, this, &ATopDownShooterCharacter::NumberKeyPressed<4>);
	inputComponent->BindKey(HotKeys[5], IE_Pressed, this, &ATopDownShooterCharacter::NumberKeyPressed<5>);
	inputComponent->BindKey(HotKeys[6], IE_Pressed, this, &ATopDownShooterCharacter::NumberKeyPressed<6>);
	inputComponent->BindKey(HotKeys[7], IE_Pressed, this, &ATopDownShooterCharacter::NumberKeyPressed<7>);
	inputComponent->BindKey(HotKeys[8], IE_Pressed, this, &ATopDownShooterCharacter::NumberKeyPressed<8>);
	inputComponent->BindKey(HotKeys[9], IE_Pressed, this, &ATopDownShooterCharacter::NumberKeyPressed<9>);
}

void ATopDownShooterCharacter::InputAxisX(float value)
{
	AxisX = value;
}

void ATopDownShooterCharacter::InputAxisY(float value)
{
	AxisY = value;
}

void ATopDownShooterCharacter::LeftShiftPressed()
{
	IsShiftPressed = true;
}

void ATopDownShooterCharacter::LeftShiftReleased()
{
	IsShiftPressed = false;
}

void ATopDownShooterCharacter::InputAttackPressed()
{
	AttackCharEvent(true);
}

void ATopDownShooterCharacter::InputAttackReleased()
{
	AttackCharEvent(false);
}

void ATopDownShooterCharacter::MovementTick(float deltaTime)
{
	if (CharacterHealthComponent && CharacterHealthComponent->IsAlive)
	{
		APlayerController* playerController = nullptr;
		if (GetController() && GetController()->IsLocalPlayerController())
		{
			playerController = Cast<APlayerController>(GetController());
		}
		else
		{
			return;
		}

		AddMovementInput(FVector(1.0f, 0.0f, 0.0f), AxisX);
		AddMovementInput(FVector(0.0f, 1.0f, 0.0f), AxisY);

		DetermineMovementState();
		
		FHitResult hitResult;
		playerController->GetHitResultUnderCursorByChannel(ETraceTypeQuery::TraceTypeQuery6, false, hitResult);
		float findRotatorResultYaw = UKismetMathLibrary::FindLookAtRotation(GetActorLocation(), hitResult.Location).Yaw;
		// TO DO: переделать на AddPitchInput по аналогии с AddMovementInput при работе с движением
		SetActorRotation(FQuat(FRotator(0.0f, findRotatorResultYaw, 0.0f)));
		SetActorRotationByYaw_OnServer(findRotatorResultYaw);

		if (CurrentWeapon)
		{
			FVector Displacement = FVector(0);
			if (IsAiming)
			{
				Displacement = FVector(0.0f, 0.0f, 160.0f);
			}
			else
			{
				Displacement = FVector(0.0f, 0.0f, 120.0f);
			}

			CurrentWeapon->UpdateStateWeapon_OnServer(GetCharacterMovement()->Velocity.Size2D(), hitResult.Location + Displacement, IsAiming);
		}
	}
}

void ATopDownShooterCharacter::AttackCharEvent(bool bIsFiring)
{
	if (CurrentWeapon)
	{
		CurrentWeapon->SetWeaponStateFire_OnServer(bIsFiring);
	}
	else
	{
		UE_LOG(LogTemp, Warning, TEXT("ATopDownShooterCharacter::AttackCharEvent - CurrentWeapon -NULL"));
	}
}

void ATopDownShooterCharacter::DetermineMovementState()
{
	FVector characterVelocity = GetVelocity();
	float direction = UKismetAnimationLibrary::CalculateDirection(GetVelocity(), GetActorRotation());

	if (IsShiftPressed && abs(direction) < 20)
	{
		SetCharacterSpeed_OnServer(MovementInfo.RunSpeed);
	}
	else
	{
		SetCharacterSpeed_OnServer(MovementInfo.WalkSpeed);
	}
}

void ATopDownShooterCharacter::InitializeWeapon(FName weaponName)
{
	FWeaponInfo* weaponInfo = InventoryComponent->Weapons.Find(weaponName);
	if (weaponInfo && weaponInfo->WeaponClass)
	{
		if (CurrentWeapon)
		{
			CurrentWeapon->Destroy();
			CurrentWeapon = nullptr;
		}

		FVector SpawnLocation = FVector(0);
		FRotator SpawnRotation = FRotator(0);

		FActorSpawnParameters SpawnParams;
		SpawnParams.SpawnCollisionHandlingOverride = ESpawnActorCollisionHandlingMethod::AlwaysSpawn;
		SpawnParams.Owner = this;
		SpawnParams.Instigator = GetInstigator();

		AWeaponBase* myWeapon = Cast<AWeaponBase>(GetWorld()->SpawnActor(weaponInfo->WeaponClass, &SpawnLocation, &SpawnRotation, SpawnParams));
		if (myWeapon)
		{
			FAttachmentTransformRules Rule(EAttachmentRule::SnapToTarget, false);
			myWeapon->AttachToComponent(GetMesh(), Rule, FName("WeaponSocketRightHand"));
			CurrentWeapon = myWeapon;

			myWeapon->WeaponSetting = *weaponInfo;
			myWeapon->CurrentRoundsInMagazine = myWeapon->MaxRoundsInMagazine;

			myWeapon->OnWeaponReloadStart.AddDynamic(this, &ATopDownShooterCharacter::WeaponReloadStart);
			myWeapon->OnWeaponReloadEnd.AddDynamic(this, &ATopDownShooterCharacter::WeaponReloadEnd);
			myWeapon->OnWeaponFireStart.AddDynamic(this, &ATopDownShooterCharacter::WeaponFireStart);
			myWeapon->TryReloadWeapon.AddDynamic(this, &ATopDownShooterCharacter::TryReloadWeapon);
		}
	}
	else
	{
		UE_LOG(LogTemp, Warning, TEXT("ATopDownShooterCharacter::InitWeapon - Weapon not found in table -NULL"));
	}
}

void ATopDownShooterCharacter::SwitchToWeapon(FName weaponName)
{
	InitializeWeapon(weaponName);
	UpdateWeaponUI(false);
}

void ATopDownShooterCharacter::StartAiming()
{
	IsAiming = true;
	CameraBoom->SetRelativeLocation(FVector(CameraAimingShift, 0.0f, 0.0f));
	SetIsAiming_OnServer(true);
}

void ATopDownShooterCharacter::StopAiming()
{
	IsAiming = false;
	CameraBoom->SetRelativeLocation(FVector(0.0f, 0.0f, 0.0f));
	SetIsAiming_OnServer(false);
}

void ATopDownShooterCharacter::SetIsAiming_OnServer_Implementation(bool aimValue)
{
	IsAiming = aimValue;
	SetIsAiming_Client(aimValue);
}

void ATopDownShooterCharacter::SetIsAiming_Client_Implementation(bool aimValue)
{
	IsAiming = aimValue;
}

void ATopDownShooterCharacter::TryReloadWeapon()
{
	if (CurrentWeapon // Есть оружие
		&& !CurrentWeapon->IsWeaponReloading // Оно не находится в стадии перезарядки
		&& CurrentWeapon->CurrentRoundsInMagazine < CurrentWeapon->MaxRoundsInMagazine) // Количество патронов в магазине меньше максимального
	{
		FAmmoInfo* ammoInfo = InventoryComponent->Ammo.Find(CurrentWeapon->WeaponSetting.WeaponName);
		if (ammoInfo && ammoInfo->CartridgesAmount > 0) // В инвентаре есть патроны
		{
			CurrentWeapon->InitReload(ammoInfo->CartridgesAmount);
		}
	}
}

void ATopDownShooterCharacter::WeaponFireStart(UAnimMontage* Anim)
{
	UpdateWeaponUI(true);
	WeaponFireStart_BP(Anim);
}

void ATopDownShooterCharacter::WeaponFireStart_BP_Implementation(UAnimMontage* Anim)
{
	// in BP
}

void ATopDownShooterCharacter::WeaponReloadStart(UAnimMontage* AnimHip, UAnimMontage* AnimIronsights)
{
	WeaponReloadStart_BP(AnimHip, AnimIronsights);
}

void ATopDownShooterCharacter::WeaponReloadEnd()
{
	FAmmoInfo* ammoInfo = InventoryComponent->Ammo.Find(CurrentWeapon->WeaponSetting.WeaponName);
	if (ammoInfo) // В инвентаре есть патроны
	{
		ammoInfo->CartridgesAmount += CurrentWeapon->CurrentRoundsInMagazine; // Возвращаем патроны в инветарь, чтобы при перезарядке не терялись патроны из магазина
		if (ammoInfo->CartridgesAmount > CurrentWeapon->MaxRoundsInMagazine)
		{
			CurrentWeapon->CurrentRoundsInMagazine = CurrentWeapon->MaxRoundsInMagazine;
			ammoInfo->CartridgesAmount -= CurrentWeapon->MaxRoundsInMagazine;
		}
		else
		{
			CurrentWeapon->CurrentRoundsInMagazine = ammoInfo->CartridgesAmount;
			ammoInfo->CartridgesAmount = 0;
		}
	}

	UpdateWeaponUI(true);
	WeaponReloadEnd_BP();
}

void ATopDownShooterCharacter::WeaponReloadStart_BP_Implementation(UAnimMontage* AnimHip, UAnimMontage* AnimIronsights)
{
	// in BP
}

void ATopDownShooterCharacter::WeaponReloadEnd_BP_Implementation()
{
	// in BP
}

void ATopDownShooterCharacter::UpdateWeaponUI(bool NeedUpdateOnlyAmmo)
{
	UpdateWeaponUI_BP(NeedUpdateOnlyAmmo);
}

void ATopDownShooterCharacter::UpdateWeaponUI_BP_Implementation(bool NeedUpdateOnlyAmmo)
{
	// in BP
}

void ATopDownShooterCharacter::HandleCartridgesAddition(FName idWeapon)
{
	if (idWeapon == CurrentWeapon->WeaponSetting.WeaponName)
	{
		UpdateWeaponUI(true);
	}
}

void ATopDownShooterCharacter::CharacterDead()
{
	CharacterDead_BP();
	if (HasAuthority())
	{
		float TimeAnim = 0.0f;
		int32 rnd = FMath::RandHelper(DeadsAnim.Num());
		UAnimInstance* animInstance = GetMesh()->GetAnimInstance();
		if (DeadsAnim[rnd] && animInstance)
		{
			TimeAnim = DeadsAnim[rnd]->GetPlayLength();
			animInstance->Montage_Play(DeadsAnim[rnd]);
		}

		UnPossessed();
		GetWorldTimerManager().SetTimer(TimerHandle_RagDollTimer, this, &ATopDownShooterCharacter::EnableRagdoll_Multicast, TimeAnim, false);
	}
	else
	{
		AttackCharEvent(false);
	}
}

void ATopDownShooterCharacter::CharacterDead_BP_Implementation()
{
	//BP
}

void ATopDownShooterCharacter::SetActorRotationByYaw_OnServer_Implementation(float Yaw)
{
	SetActorRotationByYaw_Multicast(Yaw);
}

void ATopDownShooterCharacter::SetActorRotationByYaw_Multicast_Implementation(float Yaw)
{
	if (Controller && !Controller->IsLocalPlayerController())
	{
		SetActorRotation(FQuat(FRotator(0.0f, Yaw, 0.0f)));
	}
}

void ATopDownShooterCharacter::SetCharacterSpeed_OnServer_Implementation(float speed)
{
	GetCharacterMovement()->MaxWalkSpeed = speed;
	SetCharacterSpeed_OnClient(speed);
}

void ATopDownShooterCharacter::SetCharacterSpeed_OnClient_Implementation(float speed)
{
	GetCharacterMovement()->MaxWalkSpeed = speed;
}

void ATopDownShooterCharacter::EnableRagdoll_Multicast_Implementation()
{
	if (GetMesh())
	{
		GetMesh()->SetCollisionEnabled(ECollisionEnabled::PhysicsOnly);
		GetMesh()->SetSimulatePhysics(true);
	}
}

float ATopDownShooterCharacter::TakeDamage(float DamageAmount, struct FDamageEvent const& DamageEvent, class AController* EventInstigator, AActor* DamageCauser)
{
	float ActualDamage = Super::TakeDamage(DamageAmount, DamageEvent, EventInstigator, DamageCauser);
	if (CharacterHealthComponent->IsAlive)
	{
		CharacterHealthComponent->ChangeHealth(-DamageAmount);
	}

	return ActualDamage;
}

void ATopDownShooterCharacter::AddEffect_Implementation(UStateEffectBase* effect)
{
	effect->InitObject(this);
	if (effect->VisualDisplayOfEffect)
	{
		FName Name = "Test";
		FVector Location = FVector(0,0,100);
		UNiagaraComponent* temp = UNiagaraFunctionLibrary::SpawnSystemAttached(effect->VisualDisplayOfEffect, GetMesh(), Name, Location, FRotator::ZeroRotator, EAttachLocation::SnapToTarget, false);
		effect->NiagaraComponent = temp;
	}
	effect->Start();
	Effects.Add(effect);
}

void ATopDownShooterCharacter::RemoveEffect_Implementation(UStateEffectBase* effect)
{
	int32 index;
	Effects.Find(effect, index);
	if (index != INDEX_NONE)
	{
		if (Effects[index]->NiagaraComponent)
		{
			Effects[index]->NiagaraComponent->Deactivate();
			Effects[index]->NiagaraComponent->DestroyComponent();
		}
		Effects.Remove(effect);
	}
}

void ATopDownShooterCharacter::GetLifetimeReplicatedProps(TArray<FLifetimeProperty>& OutLifetimeProps) const
{
	Super::GetLifetimeReplicatedProps(OutLifetimeProps);

	DOREPLIFETIME(ATopDownShooterCharacter, IsAiming);
	DOREPLIFETIME(ATopDownShooterCharacter, CurrentWeapon);
}