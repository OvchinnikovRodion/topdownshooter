#pragma once

#include "CoreMinimal.h"
#include "../FunctionLibrary/CustomTypes.h"
#include "InventoryComponent.generated.h"

DECLARE_DYNAMIC_MULTICAST_DELEGATE_OneParam(FOnSwitchWeapon, FName, IdWeapon);
DECLARE_DYNAMIC_MULTICAST_DELEGATE_OneParam(FOnCartridgesAdded, FName, IdWeapon);

UCLASS( ClassGroup=(Custom), meta=(BlueprintSpawnableComponent) )
class TOPDOWNSHOOTER_API UInventoryComponent : public UActorComponent
{
	GENERATED_BODY()

public:
	UInventoryComponent();
	virtual void TickComponent(float DeltaTime, ELevelTick TickType, FActorComponentTickFunction* ThisTickFunction) override;

UPROPERTY(VisibleAnywhere, BlueprintReadOnly)
	TMap<FName, FWeaponInfo> Weapons;
UPROPERTY(BlueprintAssignable, Category = "Inventory")
	FOnSwitchWeapon OnSwitchWeapon;
UFUNCTION(BlueprintCallable, Category = "Inventory")
	void AddWeapon(FName weaponName);
UFUNCTION(Server, Reliable, BlueprintCallable, Category = "Inventory")
	void SwitchToWeapon_OnServer(FName weaponName);

UPROPERTY(VisibleAnywhere, BlueprintReadOnly)
	TMap<FName, FAmmoInfo> Ammo;
UPROPERTY(BlueprintAssignable, Category = "Inventory")
	FOnCartridgesAdded OnCartridgesAdded;
UFUNCTION(BlueprintCallable, Category = "Inventory")
	void AddCartridges(FName weaponName, int quantity);

protected:
	virtual void BeginPlay() override;
};
