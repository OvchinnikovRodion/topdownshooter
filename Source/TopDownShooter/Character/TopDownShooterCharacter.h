// Copyright Epic Games, Inc. All Rights Reserved.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Character.h"
#include "../Interfaces/IEffectsSubject.h"
#include "../FunctionLibrary/CustomTypes.h"
#include "../Items/Weapons/WeaponBase.h"
#include "../StateEffects/StateEffectBase.h"
#include "InventoryComponent.h"
#include "CharacterHealthComponent.h"
#include "Components/PawnNoiseEmitterComponent.h"
#include "TopDownShooterCharacter.generated.h"

DECLARE_DYNAMIC_MULTICAST_DELEGATE(FOnUseItem);

UCLASS(Blueprintable)
class ATopDownShooterCharacter : public ACharacter, public IEffectsSubject
{
	GENERATED_BODY()

public:
	ATopDownShooterCharacter();

	virtual void Tick(float DeltaSeconds) override;
	virtual void SetupPlayerInputComponent(class UInputComponent* inputComponent) override;

	FORCEINLINE class UCameraComponent* GetTopDownCameraComponent() const { return TopDownCameraComponent; }
	FORCEINLINE class USpringArmComponent* GetCameraBoom() const { return CameraBoom; }
UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Camera")
	float CameraAimingShift = 150.0f;

UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = "Inventory")
	TObjectPtr<UInventoryComponent> InventoryComponent;
UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = "Health")
	TObjectPtr<UCharacterHealthComponent> CharacterHealthComponent;
UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Sounds")
	TObjectPtr<UPawnNoiseEmitterComponent> PawnNoiseEmitterComponent;
UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Inventory")
	TMap<int, FName> BindingWeaponsToKeys;

UFUNCTION()
	void InputAxisX(float value);
UFUNCTION()
	void InputAxisY(float value);
UFUNCTION(BlueprintCallable)
	void LeftShiftPressed();
UFUNCTION(BlueprintCallable)
	void LeftShiftReleased();
UFUNCTION()
	void InputAttackPressed();
UFUNCTION()
	void InputAttackReleased();
UFUNCTION()
	void MovementTick(float deltaTime);

	float AxisX = 0.0f;
	float AxisY = 0.0f;

UFUNCTION(BlueprintCallable)
	void DetermineMovementState();

UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Movement")
	FCharacterSpeed MovementInfo;
UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Movement")
	bool IsShiftPressed;
UPROPERTY(Replicated, EditAnywhere, BlueprintReadWrite, Category = "Aim")
	bool IsAiming;
UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Animations")
	TArray<UAnimMontage*> DeadsAnim;

	//Inventory
	template<int32 NumberKey> void NumberKeyPressed()
	{
		FName* weaponName = BindingWeaponsToKeys.Find(NumberKey);
		if (weaponName)
		{
			InventoryComponent->SwitchToWeapon_OnServer(*weaponName);
		}
	}

UPROPERTY(Replicated, EditAnywhere, BlueprintReadOnly, Category = "Weapon")
	AWeaponBase* CurrentWeapon = nullptr;
UFUNCTION(BlueprintCallable)
	void InitializeWeapon(FName IdWeapon);
UFUNCTION(BlueprintCallable)
	void SwitchToWeapon(FName IdWeapon);
UFUNCTION()
	void StartAiming();
UFUNCTION()
	void StopAiming();
UFUNCTION(Server, Reliable)
	void SetIsAiming_OnServer(bool aimValue);
UFUNCTION(Client, Reliable)
	void SetIsAiming_Client(bool aimValue);
UFUNCTION(BlueprintCallable)
	void AttackCharEvent(bool bIsFiring);
UFUNCTION()
	void WeaponFireStart(UAnimMontage* Anim);
UFUNCTION(BlueprintNativeEvent)
	void WeaponFireStart_BP(UAnimMontage* Anim);
UFUNCTION(BlueprintCallable)
	void TryReloadWeapon();
UFUNCTION()
	void WeaponReloadStart(UAnimMontage* AnimHip, UAnimMontage* AnimIronsights);
UFUNCTION(BlueprintNativeEvent)
	void WeaponReloadStart_BP(UAnimMontage* AnimHip, UAnimMontage* AnimIronsights);
UFUNCTION()
	void WeaponReloadEnd();
UFUNCTION(BlueprintNativeEvent, BlueprintCallable)
	void WeaponReloadEnd_BP();
UFUNCTION()
	void HandleCartridgesAddition(FName idWeapon);
UFUNCTION()
	void UpdateWeaponUI(bool NeedUpdateOnlyAmmo);
UFUNCTION(BlueprintNativeEvent, BlueprintCallable)
	void UpdateWeaponUI_BP(bool NeedUpdateOnlyAmmo);

FTimerHandle TimerHandle_RagDollTimer;

UFUNCTION()
	void CharacterDead();
UFUNCTION(BlueprintNativeEvent)
	void CharacterDead_BP();
UFUNCTION(NetMulticast, Reliable)
	void EnableRagdoll_Multicast();

UFUNCTION(Server, Unreliable)
	void SetActorRotationByYaw_OnServer(float Yaw);
UFUNCTION(NetMulticast, Unreliable)
	void SetActorRotationByYaw_Multicast(float Yaw);
UFUNCTION(Server, Reliable)
	void SetCharacterSpeed_OnServer(float speed);
UFUNCTION(Client, Reliable)
	void SetCharacterSpeed_OnClient(float speed);

	virtual float TakeDamage(float DamageAmount, struct FDamageEvent const& DamageEvent, class AController* EventInstigator, AActor* DamageCauser) override;

	//IEffectsSubject
	void AddEffect_Implementation(UStateEffectBase* effect) override;
	void RemoveEffect_Implementation(UStateEffectBase* effect) override;

protected:
	TArray<UStateEffectBase*> Effects;

	virtual void BeginPlay() override;

private:
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = Camera, meta = (AllowPrivateAccess = "true"))
		class UCameraComponent* TopDownCameraComponent;
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = Camera, meta = (AllowPrivateAccess = "true"))
		class USpringArmComponent* CameraBoom;
};

