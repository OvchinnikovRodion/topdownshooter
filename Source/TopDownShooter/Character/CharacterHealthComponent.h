#pragma once

#include "CoreMinimal.h"
#include "../Character/HealthComponent.h"
#include "CharacterHealthComponent.generated.h"

DECLARE_DYNAMIC_MULTICAST_DELEGATE_TwoParams(FOnShieldChange, float, Shield, float, Damage);

UCLASS(ClassGroup = (Custom), meta = (BlueprintSpawnableComponent))
class TOPDOWNSHOOTER_API UCharacterHealthComponent : public UHealthComponent
{
	GENERATED_BODY()

public:
	UPROPERTY(BlueprintAssignable, Category = "Health")
		FOnShieldChange OnShieldChange;

	FTimerHandle TimerHandle_CollDownShieldTimer;
	FTimerHandle TimerHandle_ShieldRecoveryRateTimer;

	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = "Shield")
		float CoolDownShieldRecoverTime = 5.0f;
	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = "Shield")
		float ShieldRecoverValue = 1.0f;
	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = "Shield")
		float ShieldRecoverRate = 0.1f;
	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = "Shield")
		float Shield = 100.0f;

	void ChangeHealth(float ChangeValue) override;
	void ChangeHealth(float ChangeValue, bool IgnoreShield);
	void ChangeShieldValue(float ChangeValue);
	void CoolDownShieldEnd();
	void RecoveryShield();
	UFUNCTION(BlueprintCallable)
		float GetShieldValue();
};
