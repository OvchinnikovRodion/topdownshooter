#pragma once

#include "CoreMinimal.h"
#include "../StateEffects/StateEffectBase.h"
#include "IEffectsSubject.generated.h"

UINTERFACE()
class UEffectsSubject : public UInterface
{
    GENERATED_BODY()
};

class IEffectsSubject
{
    GENERATED_BODY()

public:
    UFUNCTION(BlueprintCallable, BlueprintNativeEvent, Category = "Effects")
    void AddEffect(UStateEffectBase* effect);
    UFUNCTION(BlueprintCallable, BlueprintNativeEvent, Category = "Effects")
    void RemoveEffect(UStateEffectBase* effect);
};