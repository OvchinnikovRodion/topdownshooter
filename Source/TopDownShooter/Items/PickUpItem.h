#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "../Character/TopDownShooterCharacter.h"
#include "PickUpItem.generated.h"

class USphereComponent;

UCLASS()
class TOPDOWNSHOOTER_API APickUpItem : public AActor
{
	GENERATED_BODY()
	
public:	
	APickUpItem();

	virtual void Tick(float DeltaTime) override;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Collision")
		USphereComponent* CollisionComponent;

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
		USceneComponent* MeshContainer;

protected:
	virtual void BeginPlay() override;
};
