#include "WeaponBase.h"
#include "Engine/StaticMeshActor.h"
#include "Kismet/KismetMathLibrary.h"
#include "Kismet/GameplayStatics.h"
#include "Net/UnrealNetwork.h"
#include "NiagaraFunctionLibrary.h"

int32 DebugShowProjectileTrajectory = 1;
FAutoConsoleVariableRef CVAREShowProjectileTrajectory(
	TEXT("WeaponBase.ShowProjectileTrajectory"),
	DebugShowProjectileTrajectory,
	TEXT("Draw trajectory for projectile"),
	ECVF_Cheat);

AWeaponBase::AWeaponBase()
{
	PrimaryActorTick.bCanEverTick = true;

	SceneComponent = CreateDefaultSubobject<USceneComponent>(TEXT("Scene"));
	RootComponent = SceneComponent;

	SkeletalMeshWeapon = CreateDefaultSubobject<USkeletalMeshComponent>(TEXT("Skeletal Mesh"));
	SkeletalMeshWeapon->SetGenerateOverlapEvents(false);
	SkeletalMeshWeapon->SetCollisionProfileName(TEXT("NoCollision"));
	SkeletalMeshWeapon->SetupAttachment(RootComponent);

	StaticMeshWeapon = CreateDefaultSubobject<UStaticMeshComponent>(TEXT("Static Mesh "));
	StaticMeshWeapon->SetGenerateOverlapEvents(false);
	StaticMeshWeapon->SetCollisionProfileName(TEXT("NoCollision"));
	StaticMeshWeapon->SetupAttachment(RootComponent);

	ShootLocation = CreateDefaultSubobject<UArrowComponent>(TEXT("ShootLocation"));
	ShootLocation->SetupAttachment(RootComponent);

	//NetWork 
	bReplicates = true;
}

void AWeaponBase::BeginPlay()
{
	Super::BeginPlay();

	WeaponInit();
}

void AWeaponBase::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

	FireTick(DeltaTime);
	ReloadTick(DeltaTime);
	DispersionTick(DeltaTime);
	MagazineDropTick(DeltaTime);
	ShellDropTick(DeltaTime);
}

void AWeaponBase::FireTick(float DeltaTime)
{
	FireTimer -= DeltaTime;
	if (CurrentRoundsInMagazine > 0)
	{
		if (IsWeaponFiring)
		{
			if (FireTimer < 0.0f)
			{
				Fire();
			}
		}
	}
	else
	{
		TryReloadWeapon.Broadcast();
	}
}

void AWeaponBase::ReloadTick(float DeltaTime)
{
	if (IsWeaponReloading)
	{
		if (ReloadTimer < 0.0f)
		{
			FinishReload();
		}
		else
		{
			ReloadTimer -= DeltaTime;
		}
	}
}

void AWeaponBase::DispersionTick(float DeltaTime)
{
	if (!IsWeaponReloading)
	{
		if (!IsWeaponFiring)
		{
			if (ShouldReduceDispersion)
			{
				CurrentDispersion = CurrentDispersion - CurrentDispersionReduction;
			}
			else
			{
				CurrentDispersion = CurrentDispersion + CurrentDispersionReduction;
			}
		}

		CurrentDispersion = FMath::Clamp(CurrentDispersion, CurrentDispersionMin, CurrentDispersionMax);
		/*if (DebugShowProjectileTrajectory == 1)
		{
			UE_LOG(LogTemp, Warning, TEXT("Dispersion: MAX = %f. MIN = %f. Current = %f"), CurrentDispersionMax, CurrentDispersionMin, CurrentDispersion);
		}*/
	}
}

void AWeaponBase::MagazineDropTick(float DeltaTime)
{
	if (NeedDropMagazine)
	{
		if (DropMagazineTimer < 0.0f)
		{
			NeedDropMagazine = false;
			InitDropMesh(WeaponSetting.MagazineDropInfo.DropMesh,
				WeaponSetting.MagazineDropInfo.DropMeshOffset,
				WeaponSetting.MagazineDropInfo.DropMeshImpulseDirection,
				WeaponSetting.MagazineDropInfo.DropMeshLifeTime,
				WeaponSetting.MagazineDropInfo.ImpulsePowerDispersion,
				WeaponSetting.MagazineDropInfo.PowerImpulse,
				WeaponSetting.MagazineDropInfo.CustomMass);
		}
		else
		{
			DropMagazineTimer -= DeltaTime;
		}
	}
}

void AWeaponBase::ShellDropTick(float DeltaTime)
{
	if (NeedDropShell)
	{
		if (DropShellTimer < 0.0f)
		{
			NeedDropShell = false;
			InitDropMesh(WeaponSetting.ShellInfo.DropMesh,
				WeaponSetting.ShellInfo.DropMeshOffset,
				WeaponSetting.ShellInfo.DropMeshImpulseDirection,
				WeaponSetting.ShellInfo.DropMeshLifeTime,
				WeaponSetting.ShellInfo.ImpulsePowerDispersion,
				WeaponSetting.ShellInfo.PowerImpulse,
				WeaponSetting.ShellInfo.CustomMass);
		}
		else
		{
			DropShellTimer -= DeltaTime;
		}
	}
}

void AWeaponBase::WeaponInit()
{
	//Use GetSkinnedAsset()/SetSkinnedAsset() or GetSkeletalMeshAsset/SetSkeletalMeshAsset() instead of SkeletalMesh
	if (SkeletalMeshWeapon && !SkeletalMeshWeapon->SkeletalMesh)
	{
		SkeletalMeshWeapon->DestroyComponent(true);
	}

	if (StaticMeshWeapon && !StaticMeshWeapon->GetStaticMesh())
	{
		StaticMeshWeapon->DestroyComponent();
	}
}

void AWeaponBase::SetWeaponStateFire_OnServer_Implementation(bool bIsFire)
{
	if (CheckWeaponCanFire())
	{
		IsWeaponFiring = bIsFire;
	}
	else
	{
		IsWeaponFiring = false;
		FireTimer = 0.0f;
	}
}

bool AWeaponBase::CheckWeaponCanFire()
{
	return !IsFiringBlocked;
}

void AWeaponBase::Fire()
{
	Fire_OnServer();
}

void AWeaponBase::Fire_OnServer_Implementation()
{
	FireTimer = WeaponSetting.RateOfFire;
	CurrentRoundsInMagazine = CurrentRoundsInMagazine - 1;
	ChangeDispersionByShot();
	
	int8 NumberProjectile = GetNumberProjectileByShot();
	if (ShootLocation)
	{
		Fire_OnClient();
		FVector SpawnLocation = ShootLocation->GetComponentLocation();
		FRotator SpawnRotation = ShootLocation->GetComponentRotation();

		FVector EndLocation;
		for (int8 i = 0; i < NumberProjectile; ++i)
		{
			EndLocation = GetFireEndLocation();

			if (WeaponSetting.ProjectileSetting.Projectile)
			{
				FVector Dir = EndLocation - SpawnLocation;
				Dir.Normalize();

				FMatrix myMatrix(Dir, FVector(0, 1, 0), FVector(0, 0, 1), FVector::ZeroVector);
				SpawnRotation = myMatrix.Rotator();

				FActorSpawnParameters SpawnParams;
				SpawnParams.SpawnCollisionHandlingOverride = ESpawnActorCollisionHandlingMethod::AlwaysSpawn;
				SpawnParams.Owner = GetOwner();
				SpawnParams.Instigator = GetInstigator();

				AProjectileBase* myProjectile = Cast<AProjectileBase>(GetWorld()->SpawnActor(WeaponSetting.ProjectileSetting.Projectile, &SpawnLocation, &SpawnRotation, SpawnParams));
				if (myProjectile)
				{
					myProjectile->InitProjectile(WeaponSetting.ProjectileSetting);
				}
			}
			else
			{
				FHitResult Hit;
				TArray<AActor*> Actors;

				UKismetSystemLibrary::LineTraceSingle(
					GetWorld(),
					SpawnLocation,
					EndLocation * WeaponSetting.DistanceTrace,
					ETraceTypeQuery::TraceTypeQuery4,
					false,
					Actors,
					EDrawDebugTrace::ForDuration,
					Hit,
					true,
					FLinearColor::Red,
					FLinearColor::Green,
					5.0f);

				if (DebugShowProjectileTrajectory == 1)
				{
					DrawDebugLine(GetWorld(), SpawnLocation, SpawnLocation + ShootLocation->GetForwardVector() * WeaponSetting.DistanceTrace, FColor::Black, false, 0.0f, (uint8)'\000', 0.5f);
				}

				if (Hit.GetActor() && Hit.PhysMaterial.IsValid())
				{
					EPhysicalSurface mySurfacetype = UGameplayStatics::GetSurfaceType(Hit);

					if (WeaponSetting.ProjectileSetting.HitDecals.Contains(mySurfacetype))
					{
						UMaterialInterface* myMaterial = WeaponSetting.ProjectileSetting.HitDecals[mySurfacetype];

						if (myMaterial && Hit.GetComponent())
						{
							UGameplayStatics::SpawnDecalAttached(myMaterial, FVector(6.0f), Hit.GetComponent(), NAME_None, Hit.ImpactPoint, Hit.ImpactNormal.Rotation(), EAttachLocation::KeepWorldPosition, 10.0f);
						}
					}
					if (WeaponSetting.ProjectileSetting.HitFXs.Contains(mySurfacetype))
					{
						UParticleSystem* myParticle = WeaponSetting.ProjectileSetting.HitFXs[mySurfacetype];
						if (myParticle)
						{
							UGameplayStatics::SpawnEmitterAtLocation(GetWorld(), myParticle, FTransform(Hit.ImpactNormal.Rotation(), Hit.ImpactPoint, FVector(1.0f)));
						}
					}

					if (WeaponSetting.ProjectileSetting.HitSound)
					{
						UGameplayStatics::PlaySoundAtLocation(GetWorld(), WeaponSetting.ProjectileSetting.HitSound, Hit.ImpactPoint);
					}
				}
			}
		}
	}
	
}

void AWeaponBase::Fire_OnClient_Implementation()
{
	UGameplayStatics::SpawnSoundAtLocation(GetWorld(), WeaponSetting.SoundFireWeapon, ShootLocation->GetComponentLocation());
	UNiagaraFunctionLibrary::SpawnSystemAtLocation(GetWorld(), WeaponSetting.EffectFireWeapon, ShootLocation->GetComponentLocation());

	if (WeaponSetting.AnimationsInfo.AnimCharFire)
	{
		OnWeaponFireStart.Broadcast(WeaponSetting.AnimationsInfo.AnimCharFire);
	}

	if (WeaponSetting.ShellInfo.DropMesh)
	{
		NeedDropShell = true;
		DropShellTimer = WeaponSetting.ShellInfo.DropMeshTime;
	}
}

void AWeaponBase::UpdateStateWeapon_OnServer_Implementation(int characterSpeed, FVector shootEndLocation, bool isAiming)
{
	IsFiringBlocked = false;
	ShouldReduceDispersion = isAiming;
	ShootEndLocation = shootEndLocation;
	// TO DO: ���������� ������� �������� ������
	if (characterSpeed > 1)
	{
		if (characterSpeed < 500)
		{
			if (isAiming)
			{
				CurrentDispersionMax = WeaponSetting.DispersionWeapon.Aim_Walk_DispersionMax;
				CurrentDispersionMin = WeaponSetting.DispersionWeapon.Aim_Walk_DispersionMin;
				CurrentDispersionRecoil = WeaponSetting.DispersionWeapon.Aim_Walk_DispersionRecoil;
				CurrentDispersionReduction = WeaponSetting.DispersionWeapon.Aim_Walk_DispersionReduction;
			}
			else
			{
				CurrentDispersionMax = WeaponSetting.DispersionWeapon.Aim_Walk_DispersionMax;
				CurrentDispersionMin = WeaponSetting.DispersionWeapon.Aim_Walk_DispersionMin;
				CurrentDispersionRecoil = WeaponSetting.DispersionWeapon.Aim_Walk_DispersionRecoil;
				CurrentDispersionReduction = WeaponSetting.DispersionWeapon.Aim_Walk_DispersionReduction;
			}
		}
		else
		{
			IsFiringBlocked = true;
			SetWeaponStateFire_OnServer(false);
		}
	}
	else
	{
		if (isAiming)
		{
			CurrentDispersionMax = WeaponSetting.DispersionWeapon.Aim_Standing_DispersionMax;
			CurrentDispersionMin = WeaponSetting.DispersionWeapon.Aim_Standing_DispersionMin;
			CurrentDispersionRecoil = WeaponSetting.DispersionWeapon.Aim_Standing_DispersionRecoil;
			CurrentDispersionReduction = WeaponSetting.DispersionWeapon.Aim_Standing_DispersionReduction;
		}
		else
		{
			CurrentDispersionMax = WeaponSetting.DispersionWeapon.Aim_Standing_DispersionMax;
			CurrentDispersionMin = WeaponSetting.DispersionWeapon.Aim_Standing_DispersionMin;
			CurrentDispersionRecoil = WeaponSetting.DispersionWeapon.Aim_Standing_DispersionRecoil;
			CurrentDispersionReduction = WeaponSetting.DispersionWeapon.Aim_Standing_DispersionReduction;
		}
	}
}

void AWeaponBase::ChangeDispersionByShot()
{
	CurrentDispersion = CurrentDispersion + CurrentDispersionRecoil;
}

float AWeaponBase::GetCurrentDispersion() const
{
	float Result = CurrentDispersion;
	return Result;
}

FVector AWeaponBase::ApplyDispersionToShoot(FVector DirectionShoot) const
{
	return FMath::VRandCone(DirectionShoot, GetCurrentDispersion() * PI / 180.f);
}

FVector AWeaponBase::GetFireEndLocation() const
{
	FVector EndLocation = FVector(0.f);
	FVector tmpV = (ShootLocation->GetComponentLocation() - ShootEndLocation);

	if (tmpV.Size() > SizeVectorToChangeShootDirectionLogic)
	{
		EndLocation = ShootLocation->GetComponentLocation() + ApplyDispersionToShoot((ShootLocation->GetComponentLocation() - ShootEndLocation).GetSafeNormal()) * -20000.0f;
		if (DebugShowProjectileTrajectory == 1)
		{
			DrawDebugCone(GetWorld(), ShootLocation->GetComponentLocation(), -(ShootLocation->GetComponentLocation() - ShootEndLocation), WeaponSetting.DistanceTrace, GetCurrentDispersion() * PI / 180.f, GetCurrentDispersion() * PI / 180.f, 32, FColor::Emerald, false, .1f, (uint8)'\000', 1.0f);
		}
	}
	else
	{
		EndLocation = ShootLocation->GetComponentLocation() + ApplyDispersionToShoot(ShootLocation->GetForwardVector()) * 20000.0f;
		if (DebugShowProjectileTrajectory == 1)
		{
			DrawDebugCone(GetWorld(), ShootLocation->GetComponentLocation(), ShootLocation->GetForwardVector(), WeaponSetting.DistanceTrace, GetCurrentDispersion() * PI / 180.f, GetCurrentDispersion() * PI / 180.f, 32, FColor::Emerald, false, .1f, (uint8)'\000', 1.0f);
		}
	}

	if (DebugShowProjectileTrajectory == 1)
	{
		//direction weapon look
		DrawDebugLine(GetWorld(), ShootLocation->GetComponentLocation(), ShootLocation->GetComponentLocation() + ShootLocation->GetForwardVector() * 500.0f, FColor::Cyan, false, 5.f, (uint8)'\000', 0.5f);
		//direction projectile must fly
		DrawDebugLine(GetWorld(), ShootLocation->GetComponentLocation(), ShootEndLocation, FColor::Red, false, 5.f, (uint8)'\000', 0.5f);
		//Direction Projectile Current fly
		DrawDebugLine(GetWorld(), ShootLocation->GetComponentLocation(), EndLocation, FColor::Black, false, 5.f, (uint8)'\000', 0.5f);
		//DrawDebugSphere(GetWorld(), ShootLocation->GetComponentLocation() + ShootLocation->GetForwardVector()*SizeVectorToChangeShootDirectionLogic, 10.f, 8, FColor::Red, false, 4.0f);
	}

	UE_LOG(LogTemp, Warning, TEXT("hitResult.Location: X = %f. Y = %f. Size = %f"), EndLocation.X, EndLocation.Y, EndLocation.Z);
	return EndLocation;
}

int8 AWeaponBase::GetNumberProjectileByShot() const
{
	return WeaponSetting.NumberProjectileByShot;
}

void AWeaponBase::InitReload(int ammoInInventory)
{
	IsWeaponReloading = true;
	ReloadTimer = WeaponSetting.ReloadTime;

	if (WeaponSetting.AnimationsInfo.AnimCharReloadHip || WeaponSetting.AnimationsInfo.AnimCharReloadIronsights)
	{
		OnWeaponReloadStart.Broadcast(WeaponSetting.AnimationsInfo.AnimCharReloadHip, WeaponSetting.AnimationsInfo.AnimCharReloadIronsights);
	}

	if (WeaponSetting.MagazineDropInfo.DropMesh)
	{
		NeedDropMagazine = true;
		DropMagazineTimer = WeaponSetting.MagazineDropInfo.DropMeshTime;
	}
}

void AWeaponBase::FinishReload()
{
	OnWeaponReloadEnd.Broadcast();
	IsWeaponReloading = false;
}

void AWeaponBase::InitDropMesh(UStaticMesh* DropMesh, FTransform Offset, FVector DropImpulseDirection, float LifeTimeMesh, float ImpulseRandomDispersion, float PowerImpulse, float CustomMass)
{
	if (DropMesh)
	{
		FVector LocalDirection = this->GetActorForwardVector() * Offset.GetLocation().X + this->GetActorRightVector() * Offset.GetLocation().Y + this->GetActorUpVector() * Offset.GetLocation().Z;
		
		FTransform Transform;
		Transform.SetLocation(GetActorLocation() + LocalDirection);
		Transform.SetScale3D(Offset.GetScale3D());
		Transform.SetRotation((GetActorRotation() + Offset.Rotator()).Quaternion());

		FActorSpawnParameters SpawnParameters;
		SpawnParameters.SpawnCollisionHandlingOverride = ESpawnActorCollisionHandlingMethod::AdjustIfPossibleButAlwaysSpawn;
		SpawnParameters.Owner = this;

		AStaticMeshActor* NewActor = nullptr;
		NewActor = GetWorld()->SpawnActor<AStaticMeshActor>(AStaticMeshActor::StaticClass(), Transform, SpawnParameters);

		if (NewActor && NewActor->GetStaticMeshComponent())
		{
			NewActor->GetStaticMeshComponent()->SetCollisionProfileName(TEXT("IgnoreOnlyPawn"));
			NewActor->GetStaticMeshComponent()->SetCollisionEnabled(ECollisionEnabled::QueryAndPhysics);
			NewActor->SetActorTickEnabled(false);
			NewActor->SetLifeSpan(LifeTimeMesh);
			NewActor->GetStaticMeshComponent()->Mobility = EComponentMobility::Movable;
			NewActor->GetStaticMeshComponent()->SetSimulatePhysics(true);
			NewActor->GetStaticMeshComponent()->SetStaticMesh(DropMesh);

			NewActor->GetStaticMeshComponent()->SetCollisionResponseToChannel(ECC_GameTraceChannel1, ECollisionResponse::ECR_Ignore);
			NewActor->GetStaticMeshComponent()->SetCollisionResponseToChannel(ECC_GameTraceChannel2, ECollisionResponse::ECR_Ignore);
			NewActor->GetStaticMeshComponent()->SetCollisionResponseToChannel(ECC_Pawn, ECollisionResponse::ECR_Ignore);

			if (CustomMass > 0.0f)
			{
				NewActor->GetStaticMeshComponent()->SetMassOverrideInKg(NAME_None, CustomMass, true);
			}

			if (!DropImpulseDirection.IsNearlyZero())
			{
				FVector FinalDirection = LocalDirection + DropImpulseDirection;

				if (!FMath::IsNearlyZero(ImpulseRandomDispersion))
				{
					FinalDirection += UKismetMathLibrary::RandomUnitVectorInConeInDegrees(LocalDirection, ImpulseRandomDispersion);
				}
				FinalDirection.GetSafeNormal(0.0001f);
				NewActor->GetStaticMeshComponent()->AddImpulse(FinalDirection * PowerImpulse);
			}
		}
	}
}

void AWeaponBase::GetLifetimeReplicatedProps(TArray<FLifetimeProperty>& OutLifetimeProps) const
{
	Super::GetLifetimeReplicatedProps(OutLifetimeProps);

	DOREPLIFETIME(AWeaponBase, WeaponSetting);
	DOREPLIFETIME(AWeaponBase, IsWeaponReloading);
	DOREPLIFETIME(AWeaponBase, ShootEndLocation);
}