#include "Grenade.h"
#include "NiagaraFunctionLibrary.h"
#include "Kismet/GameplayStatics.h"

int32 DebugShowExplosionSphere = 0;
FAutoConsoleVariableRef CVAREShowExplosionSphere(
	TEXT("Grenade.ShowExplosionSphere"),
	DebugShowExplosionSphere,
	TEXT("Draw trajectory for projectile"),
	ECVF_Cheat);

void AGrenade::BeginPlay()
{
	Super::BeginPlay();
	if (IsTimeCalculatingFromStart)
	{
		StartTimer();
	}
}

void AGrenade::BulletCollisionSphereHit(class UPrimitiveComponent* HitComp, AActor* OtherActor, UPrimitiveComponent* OtherComp, FVector NormalImpulse, const FHitResult& Hit)
{
	Super::BulletCollisionSphereHit(HitComp, OtherActor, OtherComp, NormalImpulse, Hit);
}

void AGrenade::ImpactProjectile()
{
	if (IsContactFuse)
	{
		Explode_OnServer();
	}
	else
	{
		StartTimer();
	}
}

void AGrenade::StartTimer()
{
	UWorld* world = GetWorld();
	if (world == nullptr) { return; }

	if (world->GetTimerManager().IsTimerActive(TimerHandle_ExplosionTimer)) { return; }

	world->GetTimerManager().SetTimer(TimerHandle_ExplosionTimer, this, &AGrenade::Explode_OnServer, TimeToExplosion, false);
}

void AGrenade::Explode_OnServer_Implementation()
{
	UWorld* world = GetWorld();
	if (world)
	{
		world->GetTimerManager().ClearAllTimersForObject(this);
	}

	if (DebugShowExplosionSphere == 1)
	{
		DrawDebugSphere(GetWorld(), GetActorLocation(), ProjectileSetting.ProjectileMinRadiusDamage, 12, FColor::Red, false, 12.0f);
		DrawDebugSphere(GetWorld(), GetActorLocation(), ProjectileSetting.ProjectileMaxRadiusDamage, 12, FColor::Green, false, 12.0f);
	}
	if (ProjectileSetting.ExplosionFX)
	{
		SpawnExplodeFX_Multicast(ProjectileSetting.ExplosionFX);
	}
	if (ProjectileSetting.ExplosionSound)
	{
		SpawnExplodeSound_Multicast(ProjectileSetting.ExplosionSound);
	}

	TArray<AActor*> IgnoredActor;
	UGameplayStatics::ApplyRadialDamageWithFalloff(
		GetWorld(),
		ProjectileSetting.ExplosionMaxDamage,
		ProjectileSetting.ExplosionMaxDamage * 0.2f,
		GetActorLocation(),
		ProjectileSetting.ProjectileMinRadiusDamage,
		ProjectileSetting.ProjectileMaxRadiusDamage,
		5,
		NULL,
		IgnoredActor,
		this,
		nullptr);

	this->Destroy();
}

void AGrenade::SpawnExplodeFX_Multicast_Implementation(UNiagaraSystem* FxTemplate)
{
	UE_LOG(LogTemp, Warning, TEXT("ExplodeFX: %s"), *GetActorLocation().ToString());
	UNiagaraFunctionLibrary::SpawnSystemAtLocation(GetWorld(), FxTemplate, GetActorLocation(), GetActorRotation());
}

void AGrenade::SpawnExplodeSound_Multicast_Implementation(USoundBase* ExplodeSound)
{
	UGameplayStatics::PlaySoundAtLocation(GetWorld(), ExplodeSound, GetActorLocation());
}
