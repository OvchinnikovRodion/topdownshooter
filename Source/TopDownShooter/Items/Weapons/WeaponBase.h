#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "Components/ArrowComponent.h"
#include "../../FunctionLibrary/CustomTypes.h"
#include "ProjectileBase.h"
#include "WeaponBase.generated.h"

DECLARE_DYNAMIC_MULTICAST_DELEGATE_OneParam(FOnWeaponFireStart, UAnimMontage*, Anim);
DECLARE_DYNAMIC_MULTICAST_DELEGATE_TwoParams(FOnWeaponReloadStart, UAnimMontage*, AnimHip, UAnimMontage*, AnimIronsights);
DECLARE_DYNAMIC_MULTICAST_DELEGATE(FOnWeaponReloadEnd);
DECLARE_DYNAMIC_MULTICAST_DELEGATE(FTryReloadWeapon);

UCLASS()
class TOPDOWNSHOOTER_API AWeaponBase : public AActor
{
	GENERATED_BODY()

public:
	AWeaponBase();

	FOnWeaponFireStart OnWeaponFireStart;
	FOnWeaponReloadStart OnWeaponReloadStart;
	FOnWeaponReloadEnd OnWeaponReloadEnd;
	FTryReloadWeapon TryReloadWeapon;

UPROPERTY(VisibleAnywhere, BlueprintReadOnly, meta = (AllowPrivateAccess = "true"), Category = Components)
	class USceneComponent* SceneComponent = nullptr;
UPROPERTY(VisibleAnywhere, BlueprintReadOnly, meta = (AllowPrivateAccess = "true"), Category = Components)
	class USkeletalMeshComponent* SkeletalMeshWeapon = nullptr;
UPROPERTY(VisibleAnywhere, BlueprintReadOnly, meta = (AllowPrivateAccess = "true"), Category = Components)
	class UStaticMeshComponent* StaticMeshWeapon = nullptr;
UPROPERTY(VisibleAnywhere, BlueprintReadOnly, meta = (AllowPrivateAccess = "true"), Category = Components)
	class UArrowComponent* ShootLocation = nullptr;

UPROPERTY(Replicated, EditAnywhere, BlueprintReadWrite)
	FWeaponInfo WeaponSetting;

	virtual void Tick(float DeltaTime) override;
	void FireTick(float DeltaTime);
	void ReloadTick(float DeltaTime);
	void DispersionTick(float DeltaTime);
	void MagazineDropTick(float DeltaTime);
	void ShellDropTick(float DeltaTime);

	void WeaponInit();

UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = "FireLogic")
	bool IsWeaponFiring = false;
UPROPERTY(Replicated, VisibleAnywhere, BlueprintReadOnly, Category = "ReloadLogic")
	bool IsWeaponReloading = false;

UFUNCTION(Server, Reliable, BlueprintCallable)
	void SetWeaponStateFire_OnServer(bool IsFire);
	bool CheckWeaponCanFire();

UFUNCTION(BlueprintCallable, Category = "FireLogic")
	void Fire();
UFUNCTION(Server, Reliable, BlueprintCallable, Category = "FireLogic")
	void Fire_OnServer();
UFUNCTION(NetMulticast, Unreliable, BlueprintCallable, Category = "FireLogic")
	void Fire_OnClient();

UFUNCTION(Server, Reliable)
	void UpdateStateWeapon_OnServer(int characterSpeed, FVector shootEndLocation, bool isAiming);
	void ChangeDispersionByShot();
	float GetCurrentDispersion() const;
	FVector ApplyDispersionToShoot(FVector DirectionShoot)const;

	FVector GetFireEndLocation() const;
	int8 GetNumberProjectileByShot() const;

	float FireTimer = 0.0f;
UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = "ReloadLogic")
	float ReloadTimer = 0.0f;
UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = "ReloadLogic")
	float DropMagazineTimer = 0.0f;
UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = "ShellLogic")
	float DropShellTimer = 0.0f;

	bool IsFiringBlocked = false;
	bool NeedDropMagazine = false;
	bool NeedDropShell = false;
	bool ShouldReduceDispersion = false;
	float CurrentDispersion = 0.0f;
	float CurrentDispersionMax = 1.0f;
	float CurrentDispersionMin = 0.1f;
	float CurrentDispersionRecoil = 0.1f;
	float CurrentDispersionReduction = 0.1f;

UPROPERTY(Replicated, EditAnywhere, BlueprintReadWrite, Category = "FireLogic")
	FVector ShootEndLocation = FVector(0);

UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "FireLogic")
	int CurrentRoundsInMagazine = 0;
UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "FireLogic")
	int32 MaxRoundsInMagazine = 10;

	void InitReload(int ammoInInventory);
	void FinishReload();

UFUNCTION()
	void InitDropMesh(UStaticMesh* DropMesh, FTransform Offset, FVector DropImpulseDirection, float LifeTimeMesh, float ImpulseRandomDispersion, float PowerImpulse, float CustomMass);

UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Debug")
	float SizeVectorToChangeShootDirectionLogic = 200.0f;

protected:
	virtual void BeginPlay() override;
};