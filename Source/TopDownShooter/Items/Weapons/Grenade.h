#pragma once

#include "CoreMinimal.h"
#include "ProjectileBase.h"
#include "Grenade.generated.h"

UCLASS()
class TOPDOWNSHOOTER_API AGrenade : public AProjectileBase
{
	GENERATED_BODY()

public:
	void BulletCollisionSphereHit(class UPrimitiveComponent* HitComp, AActor* OtherActor, UPrimitiveComponent* OtherComp, FVector NormalImpulse, const FHitResult& Hit) override;
	void ImpactProjectile() override;

UFUNCTION(Server, Reliable)
	void Explode_OnServer();
	void StartTimer();

UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Detonation settings")
	bool IsContactFuse = true;
UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Detonation settings")
	bool IsTimeCalculatingFromStart = false;
UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Detonation settings")
	float TimeToExplosion = 0.0f;

protected:
	virtual void BeginPlay() override;
UFUNCTION(NetMulticast, Reliable)
	void SpawnExplodeFX_Multicast(UNiagaraSystem* FxTemplate);
UFUNCTION(NetMulticast, Reliable)
	void SpawnExplodeSound_Multicast(USoundBase* ExplodeSound);

	FTimerHandle TimerHandle_ExplosionTimer;
};
