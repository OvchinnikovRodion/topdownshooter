#include "../Items/PickUpItem.h"

APickUpItem::APickUpItem()
{
	PrimaryActorTick.bCanEverTick = true;

	CollisionComponent = CreateDefaultSubobject<USphereComponent>(TEXT("SphereComponent"));
	CollisionComponent->InitSphereRadius(40.0f);
	CollisionComponent->BodyInstance.SetCollisionProfileName("OverlapOnlyPawn");

	MeshContainer = CreateDefaultSubobject<USceneComponent>(TEXT("SceneComponent"));
	MeshContainer->SetupAttachment(CollisionComponent);

	bReplicates = true;
}

void APickUpItem::BeginPlay()
{
	Super::BeginPlay();
}

void APickUpItem::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);
}
