#include "TopDownShooterGameInstance.h"

bool UTopDownShooterGameInstance::GetWeaponInfoByName(FName NameWeapon, FWeaponInfo& OutInfo)
{
	FWeaponInfo* WeaponInfoRow;

	if (WeaponInfoTable)
	{
		WeaponInfoRow = WeaponInfoTable->FindRow<FWeaponInfo>(NameWeapon, "", false);
		if (WeaponInfoRow)
		{
			OutInfo = *WeaponInfoRow;
			return true;
		}
	}
	else
	{
		UE_LOG(LogTemp, Warning, TEXT("UTopDownShooterGameInstance::GetWeaponInfoByName - WeaponTable - NULL"));
	}

	return false;
}
