#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "SpawnerBase.generated.h"

UCLASS()
class TOPDOWNSHOOTER_API ASpawnerBase : public AActor
{
	GENERATED_BODY()
	
public:
	ASpawnerBase();
	virtual void Tick(float DeltaTime) override;

UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "SpawnParameters")
	TSubclassOf<class AActor> ActorToSpawn = nullptr;
UFUNCTION(BlueprintCallable)
	virtual void Spawn();

protected:
	virtual void BeginPlay() override;
};
