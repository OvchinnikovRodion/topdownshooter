#include "SpawnerBase.h"

ASpawnerBase::ASpawnerBase()
{
	PrimaryActorTick.bCanEverTick = true;
}

void ASpawnerBase::BeginPlay()
{
	Super::BeginPlay();
}

void ASpawnerBase::Spawn()
{
	FVector SpawnLocation = GetActorLocation();
	FRotator SpawnRotation = GetActorRotation();
	FActorSpawnParameters SpawnParams;
	SpawnParams.SpawnCollisionHandlingOverride = ESpawnActorCollisionHandlingMethod::AlwaysSpawn;
	SpawnParams.Instigator = GetInstigator();

	GetWorld()->SpawnActor(ActorToSpawn, &SpawnLocation, &SpawnRotation, SpawnParams);
}

void ASpawnerBase::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

}

