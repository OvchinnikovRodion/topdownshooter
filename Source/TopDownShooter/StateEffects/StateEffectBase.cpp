#include "StateEffectBase.h"
#include "../Interfaces/IEffectsSubject.h"

void UStateEffectBase::InitObject(AActor* Actor)
{
	targetActor = Actor;
}

void UStateEffectBase::Start()
{
	UWorld* world = GetWorld();
	if (world == nullptr)
	{
		return;
	}

	world->GetTimerManager().SetTimer(TimerHandle_EffectTimer, this, &UStateEffectBase::DestroyObject, TotalDuration, false);
	Execute();

	if (!IsOneTimeEffect)
	{
		world->GetTimerManager().SetTimer(TimerHandle_ExecuteTimer, this, &UStateEffectBase::Execute, RateTime, true);
	}
}

void UStateEffectBase::Execute()
{

}

void UStateEffectBase::DestroyObject()
{
	UE_LOG(LogTemp, Warning, TEXT("UStateEffectBase::DestroyObject"));
	IEffectsSubject* actor = Cast<IEffectsSubject>(targetActor);
	if (actor)
	{
		actor->Execute_RemoveEffect(targetActor, this);
	}

	UWorld* world = GetWorld();
	if (world)
	{
		world->GetTimerManager().ClearAllTimersForObject(this);
	}

	targetActor = nullptr;
	if (this && this->IsValidLowLevel())
	{
		this->ConditionalBeginDestroy();
	}
}
