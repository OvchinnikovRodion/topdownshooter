#pragma once

#include "CoreMinimal.h"
#include "StateEffectBase.h"
#include "../Character/CharacterHealthComponent.h"
#include "BleedingEffect.generated.h"

UCLASS()
class TOPDOWNSHOOTER_API UBleedingEffect : public UStateEffectBase
{
	GENERATED_BODY()
	
public:
	UBleedingEffect();

protected:
	virtual void InitObject(AActor* Actor) override;
	virtual void Execute() override;
	virtual void DestroyObject() override;

private:
	UCharacterHealthComponent* healthComponent = nullptr;
};
