#pragma once

#include "CoreMinimal.h"
#include "StateEffectBase.h"
#include "../Character/CharacterHealthComponent.h"
#include "ArmorBuffEffect.generated.h"

UCLASS()
class UArmorBuffEffect : public UStateEffectBase
{
	GENERATED_BODY()

	UArmorBuffEffect();

protected:
	virtual void InitObject(AActor* Actor) override;
	virtual void Execute() override;
	virtual void DestroyObject() override;

private:
	UCharacterHealthComponent* healthComponent = nullptr;
	float oldCoefficient = 1.0f;
};