#include "BleedingEffect.h"
#include "NiagaraFunctionLibrary.h"

UBleedingEffect::UBleedingEffect()
{
	IsOneTimeEffect = false;
	TotalDuration = 5.0f;
	Value = 5.0f;
}

void UBleedingEffect::InitObject(AActor* Actor)
{
	Super::InitObject(Actor);

	if (Actor)
	{
		healthComponent = Cast<UCharacterHealthComponent>(targetActor->GetComponentByClass(UCharacterHealthComponent::StaticClass()));
	}
}

void UBleedingEffect::Execute()
{
	if (healthComponent)
	{
		healthComponent->ChangeHealth(-Value, true);
		UE_LOG(LogTemp, Warning, TEXT("UBleedingEffect::Execute"));
	}
}

void UBleedingEffect::DestroyObject()
{
	Super::DestroyObject();
}
