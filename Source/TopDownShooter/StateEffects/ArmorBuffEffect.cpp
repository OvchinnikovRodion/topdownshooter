#include "ArmorBuffEffect.h"

UArmorBuffEffect::UArmorBuffEffect()
{
	TotalDuration = 6.0f;
	Value = 0.1f;
}

void UArmorBuffEffect::InitObject(AActor* Actor)
{
	Super::InitObject(Actor);

	if (Actor)
	{
		healthComponent = Cast<UCharacterHealthComponent>(targetActor->GetComponentByClass(UCharacterHealthComponent::StaticClass()));
	}
}

void UArmorBuffEffect::Execute()
{
	Super::Execute();

	if (healthComponent)
	{
		oldCoefficient = healthComponent->CoefDamage;
		healthComponent->CoefDamage = Value;
		UE_LOG(LogTemp, Warning, TEXT("UArmorBuffEffect::ExecuteOnce"));
	}
}

void UArmorBuffEffect::DestroyObject()
{
	healthComponent->CoefDamage = oldCoefficient;

	Super::DestroyObject();
}