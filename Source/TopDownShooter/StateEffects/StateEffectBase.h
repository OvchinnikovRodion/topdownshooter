#pragma once

#include "CoreMinimal.h"
#include "NiagaraSystem.h"
#include "StateEffectBase.generated.h"

UCLASS(BlueprintType, Blueprintable)
class UStateEffectBase : public UObject
{
	GENERATED_BODY()

public:
	UPROPERTY(EditAnywhere, BlueprintReadWrite)
		bool IsOneTimeEffect = true;
	UPROPERTY(EditAnywhere, BlueprintReadWrite)
		float TotalDuration = 5.0f;
	UPROPERTY(EditAnywhere, BlueprintReadWrite)
		float RateTime = 1.0f;
	UPROPERTY(EditAnywhere, BlueprintReadWrite)
		float Value = 1.0f;
	UPROPERTY(EditAnywhere, BlueprintReadWrite)
		UNiagaraSystem* VisualDisplayOfEffect = nullptr;
	UPROPERTY(EditAnywhere, BlueprintReadWrite)
		UNiagaraComponent* NiagaraComponent = nullptr;

	virtual void InitObject(AActor* Actor);
	virtual void Start();

	virtual void DestroyObject();

protected:
	AActor* targetActor = nullptr;
	virtual void Execute();
	FTimerHandle TimerHandle_ExecuteTimer;
	FTimerHandle TimerHandle_EffectTimer;
};
